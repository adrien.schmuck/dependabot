{
  "$schema": "http://json-schema.org/draft-06/schema#",
  "title": "Dependabot configuration file schema",
  "definitions": {
    "allow": {
      "type": "object",
      "properties": {
        "dependency-type": {
          "description": "Type of dependency",
          "enum": [
            "direct",
            "indirect",
            "all",
            "production",
            "development"
          ]
        },
        "dependency-name": {
          "type": "string",
          "description": "Name of the dependency",
          "examples": [
            "lodash"
          ]
        }
      }
    },
    "ignore": {
      "type": "object",
      "properties": {
        "dependency-name": {
          "type": "string",
          "description": "Name of the dependency.",
          "examples": [
            "lodash"
          ]
        },
        "versions": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Use to ignore specific versions or ranges of versions.",
            "examples": [
              "^1.0.0",
              ">~> 2.0"
            ]
          }
        },
        "update-types": {
          "type": "array",
          "items": {
            "description": "Use to ignore types of updates, such as semver major, minor, or patch updates on version updates.",
            "enum": [
              "version-update:semver-major",
              "version-update:semver-minor",
              "version-update:semver-patch"
            ]
          }
        }
      },
      "required": [
        "dependency-name"
      ]
    }
  },
  "type": "object",
  "properties": {
    "version": {
      "type": "integer",
      "description": "Version of the configuration file.",
      "examples": [
        2
      ]
    },
    "fork": {
      "type": "boolean",
      "description": "Create dependency update merge requests in a forked repository.",
      "examples": [
        true
      ]
    },
    "vulnerability-alerts": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean",
          "description": "Enable or disable vulnerability alerts.",
          "examples": [
            true
          ]
        },
        "assignees": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Assignees to add to the vulnerability alert.",
            "examples": [
              "dependabot"
            ]
          }
        },
        "confidential": {
          "type": "boolean",
          "description": "Create vulnerability alert issues as confidential.",
          "examples": [
            true
          ]
        }
      }
    },
    "registries": {
      "type": "object",
      "description": "Custom registries to use when updating dependencies.",
      "additionalProperties": true
    },
    "updates": {
      "type": "array",
      "description": "List of ecosystem configurations.",
      "items": {
        "type": "object",
        "properties": {
          "package-ecosystem": {
            "description": "Package ecosystem name.",
            "enum": [
              "bundler",
              "npm",
              "gomod",
              "pip",
              "docker",
              "composer",
              "pub",
              "cargo",
              "nuget",
              "maven",
              "gradle",
              "mix",
              "terraform",
              "elm",
              "gitsubmodule"
            ]
          },
          "directory": {
            "type": "string",
            "description": "Directory containing the package manager files."
          },
          "schedule": {
            "type": "object",
            "properties": {
              "interval": {
                "description": "Schedule interval.",
                "enum": [
                  "daily",
                  "weekly",
                  "monthly"
                ]
              },
              "day": {
                "description": "Day of the week to run the schedule.",
                "enum": [
                  "monday",
                  "tuesday",
                  "wednesday",
                  "thursday",
                  "friday",
                  "saturday",
                  "sunday"
                ]
              },
              "time": {
                "type": "string",
                "description": "Time of day to run the schedule.",
                "examples": [
                  "00:00",
                  "12:00",
                  "23:59"
                ]
              },
              "timezone": {
                "type": "string",
                "description": "Timezone to run the schedule.",
                "examples": [
                  "UTC",
                  "America/New_York",
                  "Europe/Paris"
                ]
              },
              "hours": {
                "type": "string",
                "description": "Specified hours to randomly run the schedule.",
                "examples": [
                  "9-17"
                ]
              }
            },
            "required": [
              "interval"
            ]
          },
          "commit-message": {
            "type": "object",
            "properties": {
              "include": {
                "type": "string",
                "description": "Specifies that any prefix is followed by a list of the dependencies updated in the commit.",
                "examples": [
                  "scope"
                ]
              },
              "prefix": {
                "type": "string",
                "description": "Commit message prefix for all commits.",
                "examples": [
                  "chore(deps)"
                ]
              },
              "prefix-development": {
                "type": "string",
                "description": "Commit message prefix for development dependencies.",
                "examples": [
                  "chore(deps-dev)"
                ]
              },
              "trailers": {
                "type": "array",
                "items": {
                  "type": "object",
                  "description": "Commit message trailers.",
                  "additionalProperties": {
                    "type": "string"
                  },
                  "examples": [
                    {
                      "Changelog": "dependencies"
                    }
                  ]
                },
                "trailers-security": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "description": "Commit message trailers for security updates.",
                    "additionalProperties": {
                      "type": "string"
                    },
                    "examples": [
                      {
                        "Changelog": "security"
                      }
                    ]
                  }
                },
                "trailers-development": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "description": "Commit message trailers for development dependencies.",
                    "additionalProperties": true,
                    "examples": [
                      {
                        "Changelog": "dependencies-dev"
                      }
                    ]
                  }
                }
              }
            }
          },
          "allow": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/allow"
            }
          },
          "ignore": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/ignore"
            }
          },
          "pull-request-branch-name": {
            "type": "object",
            "properties": {
              "separator": {
                "type": "string",
                "description": "Separator to use between the prefix and the dependency name.",
                "examples": [
                  "-"
                ]
              },
              "prefix": {
                "type": "string",
                "description": "Prefix to use for the branch name.",
                "examples": [
                  "dependabot"
                ]
              },
              "max-length": {
                "type": "integer",
                "description": "Maximum length of the branch name.",
                "examples": [
                  64
                ]
              }
            }
          },
          "vulnerability-alerts": {
            "type": "object",
            "properties": {
              "enabled": {
                "type": "boolean",
                "description": "Enable or disable vulnerability alerts.",
                "examples": [
                  true
                ]
              },
              "assignees": {
                "type": "array",
                "items": {
                  "type": "string",
                  "description": "Assignees to add to the vulnerability alert.",
                  "examples": [
                    "dependabot"
                  ]
                }
              },
              "confidential": {
                "type": "boolean",
                "description": "Create vulnerability alert issues as confidential.",
                "examples": [
                  true
                ]
              }
            }
          },
          "assignees": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Assignees to add to the merge request.",
              "examples": [
                "dependabot"
              ]
            }
          },
          "reviewers": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Reviewers to add to the merge request.",
              "examples": [
                "dependabot"
              ]
            }
          },
          "approvers": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Approvers to add to the merge request.",
              "examples": [
                "dependabot"
              ]
            }
          },
          "labels": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Custom labels to add to the merge request.",
              "examples": [
                "dependencies"
              ]
            }
          },
          "milestone": {
            "type": "string",
            "description": "Milestone to add to the merge request.",
            "examples": [
              "1.0.0"
            ]
          },
          "vendor": {
            "type": "boolean",
            "description": "Use the vendor option to tell Dependabot to vendor dependencies when updating them.",
            "examples": [
              true
            ]
          },
          "open-pull-requests-limit": {
            "type": "integer",
            "description": "Maximum number of open pull requests.",
            "examples": [
              5
            ]
          },
          "open-security-pull-requests-limit": {
            "type": "integer",
            "description": "Maximum number of open security pull requests.",
            "examples": [
              5
            ]
          },
          "target-branch": {
            "type": "string",
            "description": "Target branch to create pull requests against. This option does not affect where application is fetching configuration from.",
            "examples": [
              "master"
            ]
          },
          "versioning-strategy": {
            "description": "Versioning strategy to use when updating dependencies.",
            "enum": [
              "auto",
              "lockfile-only",
              "widen",
              "increase",
              "increase-if-necessary"
            ]
          },
          "insecure-external-code-execution": {
            "type": "string",
            "description": "Allow external code execution for bundler, mix and pip package managers.",
            "examples": [
              "allow"
            ]
          },
          "updater-options": {
            "type": "object",
            "description": "Custom options to pass to internal dependabot-core updater classes.",
            "additionalProperties": true
          },
          "unsubscribe-from-mr": {
            "type": "boolean",
            "description": "Unsubscribe from the merge request after it is created.",
            "examples": [
              true
            ]
          },
          "auto-approve": {
            "type": "boolean",
            "description": "Automatically approve the merge request after it is created.",
            "examples": [
              true
            ]
          },
          "auto-merge": {
            "anyOf": [
              {
                "type": "boolean",
                "description": "Enable or disable auto merge for merge request.",
                "examples": [
                  true
                ]
              },
              {
                "squash": {
                  "type": "boolean",
                  "description": "Set squash option to true for merge request.",
                  "examples": [
                    true
                  ]
                },
                "delay": {
                  "type": "integer",
                  "description": "Delay in seconds before setting 'Merge when pipeline succeeds' option.",
                  "examples": [
                    10
                  ]
                },
                "merge-train": {
                  "type": "boolean",
                  "description": "Add merge request to merge train instead of merging directly.",
                  "examples": [
                    true
                  ]
                },
                "allow": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/allow"
                  }
                },
                "ignore": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/ignore"
                  }
                }
              }
            ]
          },
          "rebase-strategy": {
            "anyOf": [
              {
                "description": "Rebase strategy to use for merge request updates.",
                "enum": [
                  "auto",
                  "all",
                  "none"
                ]
              },
              {
                "type": "object",
                "properties": {
                  "strategy": {
                    "description": "Rebase strategy to use for merge request updates.",
                    "enum": [
                      "auto",
                      "all",
                      "none"
                    ]
                  },
                  "on-approval": {
                    "type": "boolean",
                    "description": "Rebase merge request on approval.",
                    "examples": [
                      true
                    ]
                  },
                  "with-assignee": {
                    "type": "string",
                    "description": "Define specific assignee that allows to automatically rebase merge request.",
                    "examples": [
                      "dependabot"
                    ]
                  }
                }
              }
            ]
          },
          "registries": {
            "anyOf": [
              {
                "const": "*"
              },
              {
                "type": "array",
                "description": "Allowed registry names from top level registries definition.",
                "items": {
                  "type": "string",
                  "examples": [
                    "npm",
                    "docker"
                  ]
                }
              }
            ]
          }
        },
        "required": [
          "package-ecosystem",
          "directory"
        ]
      },
      "minItems": 1
    }
  },
  "required": [
    "updates"
  ]
}
