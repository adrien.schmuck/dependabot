# frozen_string_literal: true

module Dependabot
  module OptionsHelper
    private

    # Validate config schema
    #
    # @param [Dry::Validation::Contract] contract
    # @param [Hash] opts
    # @return [void]
    def validate_config_options(contract, opts)
      result = contract.new.call(opts)
      raise(ContractSchemaError, ContractSchemaError.format(result)) unless result.success?
    end

    # Transform key names for options related to update allow/ignore rule options
    #
    # @param [Array] opts
    # @return [Array]
    def transform_rule_options(opts)
      opts&.map do |opt|
        {
          dependency_name: opt[:"dependency-name"],
          dependency_type: opt[:"dependency-type"],
          versions: opt[:versions],
          update_types: opt[:"update-types"]
        }.compact
      end
    end
  end
end
