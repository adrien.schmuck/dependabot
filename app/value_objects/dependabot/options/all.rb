# frozen_string_literal: true

require "yaml"
require "pathname"

module Dependabot
  module Options
    # :reek:MissingSafeMethod

    # All options contained in dependabot configuration file
    #
    class All
      include ApplicationHelper
      include OptionsHelper

      # @param [String] config dependabot.yml configuration file
      # @param [String] project name of the project
      def initialize(config, project)
        @config = config
        @project = project
        @schema = Schemas.configuration_schema
        @config_filename = Pathname.new(DependabotConfig.config_filename).basename.to_s
      end

      delegate :config_base_filename, to: DependabotConfig

      # Transform dependabot configuration file into options
      #
      # @return [Array<Hash>]
      def transform
        validate!(merged_config)
        validate_config_options(DependabotConfigContract, merged_config)
        validate_config_options(UpdatesConfigContract, merged_config.slice(:updates))

        {
          forked: yml[:fork],
          registries: Registries.new(merged_config[:registries]).transform,
          updates: merged_config[:updates].map do |configuration|
            {
              fork: yml[:fork],
              **Main.new(project, configuration, yml[:registries]).transform,
              **Branch.new(configuration).transform,
              **CommitMessage.new(configuration).transform,
              **Rules.new(configuration).transform,
              **AutoMerge.new(configuration).transform,
              **Rebase.new(configuration).transform,
              **VulnerabilityAlerts.new(yml[:"vulnerability-alerts"], configuration).transform,
              **DependencyGroups.new(configuration).transform
            }.compact
          end
        }.compact
      end

      private

      attr_reader :config, :project, :schema, :config_filename

      # Parsed dependabot yml config
      #
      # @return [Hash<Symbol, Object>]
      def yml
        @yml ||= YAML.safe_load(config, symbolize_names: true)
      end

      # Base configuration if exists
      #
      # @return [Hash]
      def base_config
        @base_config ||= begin
          return {} unless config_base_filename && File.exist?(config_base_filename)

          base = YAML.load_file(config_base_filename, symbolize_names: true)
          return {} unless base
          return base if base[:updates].nil? || base[:updates].is_a?(Hash)

          raise(ContractSchemaError, "`updates` key in base configuration `#{config_base_filename}` must be a map!")
        end
      end

      # Complete merged configuration
      #
      # @return [Hash]
      def merged_config
        @merged_config ||= base_config.deep_merge({
          **yml,
          updates: yml[:updates].map { |entry| (base_config[:updates] || {}).deep_merge(entry) }
        })
      end

      # Validate data against schema
      #
      # @param [Hash] data
      # @param [String] schema
      # @return [void]
      def validate!(config)
        errors = JSON::Validator.fully_validate(schema, config)
        return if errors.empty?

        raise(Schemas::ValidationError, errors.sort.join("\n"))
      rescue Schemas::ValidationError => e
        raise("Validation for #{config_filename} failed:\n#{e.message}") if ENV["RAISE_ON_SCHEMA_ERROR"]

        msg = "Json schema validation for #{config_filename} failed. "\
        "This will cause application failure in future releases. "\
        "If You think that this is a bug, please submit an issue at: https://gitlab.com/dependabot-gitlab/dependabot/-/issues"\
        "\nerrors:\n#{e}"

        ApplicationHelper.log(:error, msg)
      end
    end
  end
end
