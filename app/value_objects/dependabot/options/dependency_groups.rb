# frozen_string_literal: true

module Dependabot
  module Options
    # Dependency group related options
    #
    class DependencyGroups < OptionsBase
      # Transoform dependency update groups options
      #
      # @return [Hash]
      def transform
        groups = opts[:groups]
        return {} unless groups

        validate_groups_config(groups)

        { groups: groups.deep_transform_keys(&:to_s) }
      end

      private

      # Validate groups config schema
      #
      # @param [Hash] groups
      # @return [void]
      def validate_groups_config(groups)
        results = groups.transform_values { |patterns| GroupUpdatesContract.new.call(patterns) }
        return unless results.values.any?(&:failure?)

        error = results
                .select { |_name, result| result.failure? }
                .map { |name, result| "group '#{name}' has schema errors: #{ContractSchemaError.format(result, ', ')}" }
                .join("\n")
        raise(ContractSchemaError, error)
      end
    end
  end
end
