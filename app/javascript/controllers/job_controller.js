import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="job"
export default class extends Controller {
  execute({ target }) {
    const url = target.getAttribute("data-url");
    this.fetch(url);
  }

  toggle({ target }) {
    const url = target.value;
    const enabled = target.checked;

    this.fetch(`${url}?enabled=${enabled}`);
  }

  fetch(url) {
    fetch(url, {
      method: "PUT",
      headers: {
        "X-CSRF-Token": document.querySelector("meta[name=csrf-token]")?.content
      }
    })
      .then((response) => response.text())
      .then((html) => {
        const header = this.header();
        header.insertAdjacentHTML("afterend", html);
      });
  }

  header() {
    return document.getElementById("header");
  }
}
