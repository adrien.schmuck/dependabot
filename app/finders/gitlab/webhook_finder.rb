# frozen_string_literal: true

module Gitlab
  class WebhookFinder < FinderBase
    def initialize(project_name)
      @project_name = project_name
    end

    # Find project hook
    #
    # @return [Integer]
    def find
      gitlab.project_hooks(project_name)
            .auto_paginate
            .find { |hook| hook.url == "#{AppConfig.dependabot_url}/api/v2/hooks" }
            &.id
    end

    private

    attr_reader :project_name
  end
end
