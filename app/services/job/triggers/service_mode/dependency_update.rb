# frozen_string_literal: true

module Job
  module Triggers
    module ServiceMode
      module DependencyUpdate
        def call
          create_update_run

          super
        ensure
          save_execution_details
        end

        private

        # Project
        #
        # @return [Project]
        def project
          @project ||= Project.find_by(name: project_name)
        end

        # Update job
        #
        # @return [Update::Job]
        def update_job
          @update_job ||= project.update_jobs.find_or_initialize_by(
            package_ecosystem: package_ecosystem,
            directory: directory
          )
        end

        # Dependency update run
        #
        # @return [Update::Run]
        def update_run
          @update_run ||= Update::Run.create!(job: update_job)
        end
        alias_method :create_update_run, :update_run

        # Persist execution errors
        #
        # @return [void]
        def save_execution_details
          finished_at = Time.zone.now

          update_run.update_attributes!(finished_at: finished_at)
          update_run.save_errors!(UpdateFailures.fetch)
          update_run.save_log_entries!(UpdateLogs.fetch)

          project.update_attributes!(last_update_run_at: finished_at)
          project.update_attributes!(last_run_status: update_run.failed ? Update::Run::FAILED : Update::Run::SUCCESS)
        end
      end
    end
  end
end
