# frozen_string_literal: true

RSpec.describe Schemas do
  it "has valid configuration schema" do
    metaschema = JSON::Validator.validator_for_name("draft6").metaschema

    expect(JSON::Validator.validate(metaschema, described_class.configuration_schema)).to be_truthy
  end

  it "schema error message does not contain UUID" do
    errors = JSON::Validator.fully_validate(described_class.configuration_schema, { "version" => 2 })
    validation_error = Schemas::ValidationError.new(errors.join("\n"))

    expect(validation_error.message).to eq("The property '#/' did not contain a required property of 'updates'")
  end
end
