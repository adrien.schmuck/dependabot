# frozen_string_literal: true

describe Job::Triggers::NotifyRelease do
  subject(:trigger_update) do
    described_class.call(dependency_name, package_ecosystem)
  end

  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }
  let(:directory) { "/" }

  let(:project) { build(:project) }

  before do
    allow(Project).to receive(:all).and_return([project])
    allow(Dependabot::Update::Runner).to receive(:call)
  end

  context "with valid existing project" do
    it "triggers updates" do
      trigger_update

      expect(Dependabot::Update::Runner).to have_received(:call).with(
        dependency_name: dependency_name,
        package_ecosystem: package_ecosystem,
        directory: "/",
        project_name: project.name
      )
    end
  end

  context "without valid existing project" do
    let(:package_ecosystem) { "npm" }

    it "does not trigger updates" do
      trigger_update

      expect(Dependabot::Update::Runner).not_to have_received(:call)
    end
  end

  context "with errors during update" do
    before do
      allow(Dependabot::Update::Runner).to receive(:call).and_raise(StandardError)
    end

    it "fails update run" do
      expect { trigger_update }.to raise_error(
        "One or more errors occurred while updating #{dependency_name} for #{package_ecosystem}"
      )
    end
  end
end
