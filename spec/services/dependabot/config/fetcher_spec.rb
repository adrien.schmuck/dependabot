# frozen_string_literal: true

describe Dependabot::Config::Fetcher do
  subject(:fetched_config) { described_class.call(project_name) }

  include_context "with dependabot helper"

  let(:default_branch) { "main" }
  let(:config_file) { DependabotConfig.config_filename }
  let(:config) { Configuration.new(updates: updates_config, registries: registries) }

  let(:gitlab) do
    instance_double("Gitlab::Client",
                    project: Gitlab::ObjectifiedHash.new(default_branch: default_branch),
                    file_contents: raw_config)
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
  end

  context "without custom branch configuration" do
    it "fetches config from default branch" do
      expect(fetched_config).to eq(config)
      expect(gitlab).to have_received(:file_contents).with(project_name, config_file, default_branch)
    end
  end

  context "with custom branch configuration" do
    let(:branch) { "custom_branch" }

    before do
      allow(DependabotConfig).to receive(:config_branch) { branch }
    end

    it "fetches config from configured branch" do
      expect(fetched_config).to eq(config)
      expect(gitlab).to have_received(:file_contents).with(project_name, config_file, branch)
    end
  end

  context "with missing configuration" do
    let(:response_mock) do
      Gitlab::ObjectifiedHash.new(
        code: 404,
        parsed_response: "Not found",
        request: { base_uri: "gitlab.com", path: "/file_contents" }
      )
    end

    before do
      allow(gitlab).to receive(:file_contents)
        .with(project_name, DependabotConfig.config_filename, default_branch)
        .and_raise(Gitlab::Error::NotFound.new(response_mock))
    end

    context "with local configuration present" do
      before do
        allow(DependabotConfig).to receive(:config_local_filename).and_return(config_local_file)
      end

      context "when local configuration file exists" do
        let(:config_local_file) { "spec/fixture/gitlab/responses/dependabot.yml" }
        let(:local_config) do
          File.read("spec/fixture/gitlab/responses/dependabot.yml").then do |conf|
            Configuration.new(**Dependabot::Options::All.new(conf, project_name).transform)
          end
        end

        it "fetches config from local file" do
          expect(fetched_config).to eq(local_config)
        end
      end

      context "when local configuration file does not exist" do
        let(:config_local_file) { "spec/fixture/gitlab/responses/dependabot.yml.not_found" }

        it "raises MissingConfigurationError" do
          expect { fetched_config }.to raise_error(
            Dependabot::MissingConfigurationError,
            "Local configuration file #{config_local_file} not found!"
          )
        end
      end
    end

    context "without local configuration" do
      it "raises MissingConfigurationError" do
        expect { fetched_config }.to raise_error(
          Dependabot::MissingConfigurationError,
          "#{config_file} not present in #{project_name}'s branch #{default_branch}"
        )
      end
    end
  end
end
