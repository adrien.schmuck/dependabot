# frozen_string_literal: true

require_relative "../authenticated_endpoint"
require_relative "../paginated_endpoint"

describe "Users", :integration, type: :request do
  include_context "with api helper"

  api_prefix = "/api/v2/users"

  describe api_prefix do
    let(:path) { api_prefix }

    context "get" do
      before do
        allow(User).to receive(:all).and_return(User.where(username: user.username))
      end

      it "lists registered users" do
        api_get(path)

        expect_status(200)
        expect_entity_response(User::Entity, [user])
      end

      context "with pagination" do
        before do
          allow(User).to receive(:all).and_return(
            User.where(username: user.username).or(username: create(:user).username)
          )

          api_get(path)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end

    context "create" do
      let(:password) { "test-token" }
      let(:created_user) { User.find_by(username: new_user.username) }

      context "with new user" do
        let(:new_user) { build(:user) }

        it "creates user" do
          api_post(path, { username: new_user.username, password: new_user.password })

          expect_status(201)
          expect_entity_response(User::Entity, new_user)
          expect(created_user.authenticate(new_user.password)).to be_truthy
        end
      end

      context "with existing user" do
        let(:user) { create(:user) }

        it "returns user already exists error" do
          api_post(path, { username: user.username, password: "test" })

          expect_status(409)
          expect_json(error: "User already exists")
        end
      end
    end
  end

  describe "#{api_prefix}/:username" do
    # Replace dot due to issue with route not matching: https://github.com/ruby-grape/grape/issues/1889
    let!(:user) { create(:user, username: Faker::Internet.username.tr(".", "-")) }
    let(:path) { "#{api_prefix}/#{user.username}" }

    context "get" do
      it "returns user" do
        api_get(path)

        expect_status(200)
        expect_entity_response(User::Entity, user)
      end
    end

    context "update" do
      it "updates user" do
        api_put(path, { password: "new_password" })

        expect_status(200)
        expect(User.find_by(username: user.username).authenticate("new_password")).to be_truthy
      end
    end

    context "delete" do
      it "deletes user" do
        api_delete(path)

        expect_status(200)
        expect(User.where(username: user.username)).to be_empty
      end
    end
  end
end
