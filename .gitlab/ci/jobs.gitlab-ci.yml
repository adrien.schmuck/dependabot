include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

# ======================================================================================================================
# Runners
# ======================================================================================================================
.bundle_install:
  variables:
    BUNDLE_PATH: vendor/bundle
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - bundle install

.gem_cache: &gem_cache
  key:
    files:
      - Gemfile.lock
  paths:
    - vendor/bundle
  policy: pull

.node_cache: &node_cache
  key:
    prefix: node
    files:
      - package-lock.json
  paths:
    - .npm
  policy: pull

.browser_cache: &browser_cache
  key:
    prefix: playwright
    files:
      - package-lock.json
  paths:
    - $PLAYWRIGHT_BROWSERS_PATH
  policy: pull

.coverage_cache: &coverage_cache
  key: coverage-$CODACY_VERSION
  paths:
    - codacy-coverage-reporter
  policy: pull-push

.ruby_runner:
  extends: .bundle_install
  cache:
    - *gem_cache

.node_runner:
  image: registry.gitlab.com/dependabot-gitlab/ci-images/node:20
  extends: .bundle_install
  before_script:
    - npm ci --cache .npm
    - !reference [.bundle_install, before_script]
  cache:
    - *gem_cache
    - *node_cache

.docker_runner:
  image: registry.gitlab.com/dependabot-gitlab/ci-images/docker:24.0
  services:
    - name: docker:24.0-dind
      alias: docker
  tags:
    - docker
  variables:
    DOCKER_HOST: tcp://docker:2375

.rspec_runner:
  stage: test
  extends: .node_runner
  services:
    - name: bitnami/redis:7.2-debian-11
      alias: redis
    - name: bitnami/mongodb:7.0-debian-11
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    COVERAGE: "true"
    COV_DIR: reports/coverage/${CI_JOB_NAME}
  script:
    - bundle exec rspec ${RSPEC_TAGS} --format documentation --format RspecJunitFormatter --out tmp/rspec.xml
  artifacts:
    reports:
      junit: tmp/rspec.xml
    paths:
      - reports/coverage
    expire_in: 1 day
    when: always

.image_builder:
  extends: .docker_runner
  stage: build
  variables:
    QEMU_IMAGE: tonistiigi/binfmt:qemu-v7.0.0
  retry: 2 # ghcr is rather bad, so we have to retry the build most of the times when all images are built
  before_script:
    - source .gitlab/script/utils.sh
    - |
      mkdir -p $HOME/.docker
      cat <<- EOF > $HOME/.docker/config.json
      {
        "auths": {
          "$CI_REGISTRY": {
            "auth": "$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)"
          }
        }
      }
      EOF
    - |
      if [[ "${BUILD_PLATFORM}" =~ arm64 ]]; then
        log_info "Installing latest qemu emulators"
        docker pull -q ${QEMU_IMAGE};
        log "Removing existing emulator"
        docker run --rm --privileged ${QEMU_IMAGE} --uninstall qemu-aarch64,qemu-arm;
        log "Installing new emulator"
        docker run --rm --privileged ${QEMU_IMAGE} --install arm64,arm;
        log_success "done!"
      fi
    - docker buildx create --use
  script:
    - .gitlab/script/build-image.sh $APP_IMAGE_NAME $CURRENT_TAG $LATEST_TAG $IMAGE_TYPE $UPDATER_IMAGE_NAME_SEPARATOR

.snyk_runner:
  stage: static analysis
  image: registry.gitlab.com/dependabot-gitlab/ci-images/docker-snyk:24.0-1.1254
  extends:
    - .docker_runner

# ======================================================================================================================
# Jobs
# ======================================================================================================================

# ----------------------------------------------------------------------------------------------------------------------
# .pre
#
.cache_dependencies:
  stage: .pre
  extends:
    - .node_runner
  variables:
    BUNDLE_FROZEN: "true"
  script:
    - npx playwright install chromium
  cache:
    - <<: *gem_cache
      policy: pull-push
    - <<: *node_cache
      policy: pull-push
    - <<: *browser_cache
      policy: pull-push

# ----------------------------------------------------------------------------------------------------------------------
# compile stage
#
.build_docs:
  stage: compile
  extends: .node_runner
  environment:
    name: docs/$CI_COMMIT_REF_NAME
    url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/vitepress/index.html
    auto_stop_in: 1 day
  script:
    - bundle exec rake "oapi:fetch"
    - npm run "docs:build"
  artifacts:
    expire_in: 1 day
    paths:
      - vitepress

.compile_assets:
  stage: compile
  extends: .node_runner
  script:
    - bundle exec rake assets:precompile
  artifacts:
    expire_in: 1 day
    paths:
      - public/assets

# ----------------------------------------------------------------------------------------------------------------------
# build stage
#
.build_core_image:
  extends: .image_builder
  variables:
    IMAGE_TYPE: core

.build_bundler_image:
  extends: .image_builder
  variables:
    IMAGE_TYPE: bundler

.build_ecosystem_images:
  extends: .image_builder
  parallel:
    matrix:
      - IMAGE_TYPE:
          - npm
          - gomod
          - pip
          - docker
          - composer
          - pub
          - cargo
          - nuget
          - maven
          - gradle
          - mix
          - terraform
          - elm
          - gitsubmodule
  before_script:
    # skip arm64 build for nuget due to issues with qemu: https://gitlab.com/qemu-project/qemu/-/issues/249
    - |
      if [[ "$IMAGE_TYPE" == "nuget" ]]; then
        export BUILD_PLATFORM=linux/amd64
      fi
    - !reference [.image_builder, before_script]

# ----------------------------------------------------------------------------------------------------------------------
# 'static analysis' stage
#
.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning

# ----------------------------------------------------------------------------------------------------------------------
# test stage
#
.unit-test-standalone:
  extends: .rspec_runner
  variables:
    RSPEC_TAGS: --tag standalone
    SETTINGS__STANDALONE: "true"

.unit-test-service:
  extends: .rspec_runner
  variables:
    RSPEC_TAGS: --tag ~standalone
    SETTINGS__STANDALONE: "false"

.system-test:
  extends: .rspec_runner
  services:
    - name: ${MOCK_IMAGE}
      alias: smocker
    - name: bitnami/redis:7.2-debian-11
      alias: redis
    - name: bitnami/mongodb:7.0-debian-11
      alias: mongodb
  variables:
    MOCK_HOST: smocker
    GITLAB_URL: http://${MOCK_HOST}:8080
    RSPEC_TAGS: --tag system
  before_script:
    - !reference [.ruby_runner, before_script]
    - .gitlab/script/build-core-helpers.sh bundler

.e2e-test:
  stage: test
  image: registry.gitlab.com/dependabot-gitlab/ci-images/docker-node:24.0-20
  extends:
    - .docker_runner
    - .node_runner
  variables:
    MOCK_HOST: docker
    APP_HOST: docker
    COMPOSE_PROJECT_NAME: dependabot
    SETUP_E2E_ENVIRONMENT: "true"
    UPDATER_IMAGE_PATTERN: ${APP_IMAGE_NAME}/%<package_ecosystem>s:${CURRENT_TAG}
  script:
    - npx playwright install chromium
    - npm run "e2e:test"
  after_script:
    - echo -e "\e[0Ksection_start:`date +%s`:log_deploy[collapsed=true]\r\e[0KEnv logs"
    - .gitlab/script/log-deploy.sh
    - echo -e "\e[0Ksection_end:`date +%s`:log_deploy\r\e[0K"
  cache:
    - *gem_cache
    - *node_cache
    - *browser_cache
  artifacts:
    paths:
      - tmp/allure-results
    reports:
      junit: tmp/playwright.xml
    expire_in: 1 day
    when: always

.migration-test:
  extends: .node_runner
  services:
    - name: bitnami/mongodb:7.0-debian-11
      alias: mongodb
  variables:
    BUNDLE_PATH: $CI_PROJECT_DIR/vendor/bundle
    MONGODB_URL: mongodb:27017
  script:
    - .gitlab/script/test-migration.sh

.standalone-test:
  stage: test
  extends: .docker_runner
  variables:
    COMPOSE_PROJECT_NAME: dependabot
    METRICS_REPORT: metrics.txt
  script:
    - .gitlab/script/run-standalone.sh
  after_script:
    - curl -X POST -s "http://docker:8081/sessions/verify" | jq
    - |
      echo "# TYPE update_duration_seconds_sum summary" > $METRICS_REPORT
      echo "update_duration_seconds_sum $(cat time.txt)" >> $METRICS_REPORT
  artifacts:
    expire_in: 1 day
    when: always
    reports:
      metrics: $METRICS_REPORT

# ----------------------------------------------------------------------------------------------------------------------
# test report stage
#
.coverage:
  stage: report
  extends: .node_runner
  variables:
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    NO_COLOR: 1
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
  when: always
  before_script:
    - .gitlab/script/download-coverage.sh
    - !reference [.node_runner, before_script]
  script:
    - bundle exec rake "ci:merge_coverage"
    - |
      ./codacy-coverage-reporter report \
        --commit-uuid ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-$CI_COMMIT_SHA} \
        --coverage-reports coverage/coverage.xml \
        --language ruby
  cache:
    - *gem_cache
    - *node_cache
    - *coverage_cache
  artifacts:
    expire_in: 1 day
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

.e2e-report:
  stage: report
  image:
    name: tobix/allure-cli
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
  environment:
    name: e2e-report/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/tmp/allure-report/index.html
    auto_stop_in: 1 day
  script:
    - allure generate --single-file -c -o tmp/allure-report tmp/allure-results
  artifacts:
    paths:
      - tmp/allure-report
    expire_in: 1 day
    when: always

# ----------------------------------------------------------------------------------------------------------------------
# release stage
#
.release_image:
  stage: release
  image:
    name: quay.io/containers/skopeo:v1.14.0
    entrypoint: [""]
  variables:
    DOCKERHUB_IMAGE: docker.io/andrcuns/dependabot-gitlab
    GITHUB_IMAGE: ${CONTAINER_REGISTRY}/dependabot-gitlab/dependabot-gitlab
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | skopeo login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - echo "$CONTAINER_REGISTRY_PASSWORD" | skopeo login --username $CONTAINER_REGISTRY_USER --password-stdin $CONTAINER_REGISTRY
    - echo "$DOCKERHUB_PASSWORD" | skopeo login --username "$DOCKERHUB_USERNAME" --password-stdin docker.io
  script:
    - .gitlab/script/release-image.sh
  interruptible: false

.gitlab_release:
  image: registry.gitlab.com/dependabot-gitlab/ci-images/release-cli:0.16
  extends: .ruby_runner
  stage: release
  variables:
    CHANGELOG_FILE: release_notes.md
    RAILS_ENV: production # workaround to skip js runtime check
  script:
    - bundle exec rake "release:changelog[$CI_COMMIT_TAG,$CHANGELOG_FILE]"
  release:
    tag_name: $CI_COMMIT_TAG
    description: $CHANGELOG_FILE
  interruptible: false

.update_chart:
  extends: .node_runner
  stage: release
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"
  interruptible: false

.update_standalone:
  extends: .node_runner
  stage: release
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"
  interruptible: false

.publish_docs:
  stage: release
  image: eeacms/rsync:2.4
  script:
    - rsync -ravh --delete vitepress/ public/
  interruptible: false
  artifacts:
    expire_in: 1 day
    paths:
      - public

.bump_version:
  extends: .node_runner
  stage: release
  variables:
    GIT_PUSH_TOKEN: $GITLAB_ACCESS_TOKEN
  before_script:
    - !reference [.node_runner, before_script]
    - source .gitlab/script/utils.sh
    - setup_git
  script:
    - bundle exec rake "release:app[$VERSION_COMPONENT]"
    - git push origin $CI_COMMIT_BRANCH
    - git push origin --tags
  interruptible: false

# ----------------------------------------------------------------------------------------------------------------------
# deploy stage
#
.deploy:
  stage: deploy
  inherit:
    variables:
      - APP_IMAGE_NAME
      - CURRENT_TAG
  trigger:
    include: .gitlab/ci/deploy/main.gitlab-ci.yml
    strategy: depend
