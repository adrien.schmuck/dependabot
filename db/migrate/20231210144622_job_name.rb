# frozen_string_literal: true

class JobName < Mongoid::Migration
  def self.up
    Update::Job.all.each do |job|
      job.update!(name: "#{job.project.name}:#{job.package_ecosystem}:#{job.directory}")
    end

    Update::Job.create_indexes
  end

  def self.down; end
end
